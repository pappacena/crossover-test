from django.db import models
from datafetcher.tasks import send_mail
import logging
logger = logging.getLogger()

METRICS = [
    (1, "CPU (%)"),
    (2, "Used memory (%)"),
    (3, "Uptime (secs)")
]

# API translation codes for metrics
METRIC_CODES = {
    "cpu": 1,
    "memory": 2,
    "uptime": 3
}


class Machine(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    ip_addr = models.CharField(max_length=255,
                               help_text="Machine addres or name")
    contact_email = models.EmailField(null=True)

    def sync_config(self, client_config):
        """Sync this database Machine object with
        the given configparser.ClientConfig object"""
        self.contact_email = client_config.email
        self.save()
        self.alarmthreshold_set.all().delete()
        for metric, threshold in client_config.get_alarms().items():
            AlarmThreshold.objects.create(
                machine=self,
                metric=METRIC_CODES[metric],
                value=threshold
            )

    def check_alerts(self, measure):
        """Check configured alarms and send email if necessary"""
        for alarm in self.alarmthreshold_set.filter(metric=measure.metric):
            if measure.value > alarm.value:
                logger.info("Triggering alarm for %s" % self)
                self.send_alarm_email(measure, alarm)

    def send_alarm_email(self, measure, alarm):
        metric_name = dict(METRICS)[measure.metric]
        message = "Alarm: {metric} value is {value} (> {threshold})".format(
            metric=metric_name,
            value=measure.value,
            threshold=alarm.value
        )
        subject = "Machine %s - %s alarm!" % (self, metric_name)
        from_email = "pappacena@gmail.com"
        logger.info("Sending email for %s: %s", self, subject)
        send_mail.apply_async((subject, message, from_email,
                               [self.contact_email]))

    def __str__(self):
        return self.name or self.ip_addr


class AlarmThreshold(models.Model):
    machine = models.ForeignKey(Machine)
    metric = models.IntegerField(choices=METRICS)
    value = models.FloatField()


class NumericMeasure(models.Model):
    collected_at = models.DateTimeField(
        auto_now_add=True,
        help_text="When this measurement was sent to server"
    )
    machine = models.ForeignKey(Machine)
    metric = models.IntegerField(choices=METRICS)
    value = models.FloatField()


class WinEvent(models.Model):
    collected_at = models.DateTimeField(
        auto_now_add=True,
        help_text="When this event was sent to server"
    )
    date = models.DateTimeField(help_text="When the event actually happened")
    machine = models.ForeignKey(Machine)
    category = models.CharField(max_length=255)
    identifier = models.CharField(
        max_length=255,
        help_text="Local identifier of the event in the source machine"
    )
    message = models.TextField()
