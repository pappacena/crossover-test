from django.conf.urls import url
from .views import save_measure

urlpatterns = [
    url(r'^api/v1/measure/?$', save_measure),
]
