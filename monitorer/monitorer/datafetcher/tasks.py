from django.core.mail import send_mail as django_send_mail
from celery import task


@task
def send_mail(*args, **kwargs):
    django_send_mail(*args, **kwargs)
