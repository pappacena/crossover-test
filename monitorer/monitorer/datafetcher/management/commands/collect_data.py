from django.core.management.base import BaseCommand
from datafetcher.configparser import ConfigParser
from datafetcher.remoterunner import RemoteRunner
from datafetcher.models import Machine
import gevent


class Command(BaseCommand):
    args = ''
    help = 'Run the monitoring script for all machines'

    def add_arguments(self, parser):
        parser.add_argument(
            '--config',
            action=None,
            required=True,
            dest='configfile',
            default=None,
            help='Path to configuration file')

    def handle(self, *args, **options):
        """TODO: Write unit tests"""
        self.run_on_all_machines(options["configfile"])

    def run_on_all_machines(self, cfgfile):
        """TODO: Write unit tests"""
        with open(cfgfile) as fd:
            tasks = []
            for client in ConfigParser(fd):
                machine = Machine.objects.get_or_create(ip_addr=client.ip)[0]
                machine.sync_config(client)
                self.stdout.write("Starting execution for machine %s\n"
                                  % machine)
                tasks.append(gevent.spawn(self.execute_remote_steps, client))

        self.stdout.write("Waiting %s tasks to finish" % len(tasks))
        if len(tasks):
            gevent.wait(tasks)

    def execute_remote_steps(self, client):
        """TODO: Write unit tests"""
        r = RemoteRunner(
            client.ip,
            int(client.port or 22),
            client.username,
            client.password
        )
        self.stdout.write("Executing steps for %s" % client.ip)
        r.execute_steps()
