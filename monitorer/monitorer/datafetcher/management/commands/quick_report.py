from django.core.management.base import BaseCommand
from datafetcher.models import Machine, METRICS


class Command(BaseCommand):
    args = ''
    help = 'Show the latest measures for all machines'

    def handle(self, *args, **options):
        """TODO: Write unit tests"""
        self.stdout.write("\n\tShowing the latest state of all machines\n\n")
        for machine in Machine.objects.order_by("ip_addr"):
            self.stdout.write("====")
            self.stdout.write("Machine: %s" % machine)
            for metric_id, metric_name in METRICS:
                measure = machine.numericmeasure_set.filter(
                    metric=metric_id
                ).latest("collected_at")
                self.stdout.write("%s: %.2f" % (metric_name, measure.value))
