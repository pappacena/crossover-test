"""Helper functions to encrypt/decrypt and get json from request using
our cryptography algorithm and key"""
from Crypto.Cipher import AES
from django.conf import settings
import json
import math


def _add_padding(string):
    return string.ljust(16 * math.ceil(len(string) / 16))


def encrypt(data):
    c = AES.new(settings.DATAFETCHER_CRYPT_KEY)
    return c.encrypt(_add_padding(json.dumps(data)))


def decrypt(data):
    c = AES.new(settings.DATAFETCHER_CRYPT_KEY)
    return json.loads(c.decrypt(data))


def get_json(request):
    return decrypt(request.body)
