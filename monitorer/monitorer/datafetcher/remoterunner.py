import paramiko
from django.conf import settings
import os


class CommandError(ValueError):
    pass


class RemoteRunner:
    REMOTE_PATH = "/tmp/datafetcher_script/"
    VENV_PATH = "/tmp/datafetcher_venv"
    OS_PACKAGES = "build-essential python-dev python-virtualenv"

    def __init__(self, host, port, username, password):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self._connection = None

    @property
    def connection(self):
        if self._connection is None:
            self._connection = paramiko.SSHClient()
            self._connection.set_missing_host_key_policy(
                paramiko.WarningPolicy()
            )
            self._connection.connect(
                self.host, port=self.port,
                username=self.username,
                password=self.password
            )
        return self._connection

    def run_cmd(self, cmd, get_pty=True):
        stdin, stdout, stderr = self.connection.exec_command(
            cmd,
            get_pty=get_pty
        )
        exit_status = stdout.channel.recv_exit_status()
        if exit_status != 0:
            raise CommandError("stderr: %s \n --- \nstdout: %s" % (
                stderr.read(),
                stdout.read()
            ))

    def prepare_directory(self):
        cmds = ["rm -rf %s %s" % (self.REMOTE_PATH, self.VENV_PATH),
                "mkdir -p %s" % self.REMOTE_PATH]
        for cmd in cmds:
            self.run_cmd(cmd)

    def upload_client(self):
        """TODO: Write unit tests"""
        basepath = settings.DATAFETCHER_CLIENT_PATH
        sftp = self.connection.open_sftp()
        files = [
            os.path.join(basepath, "requirements.txt"),
            os.path.join(basepath, "src", "main.py"),
            os.path.join(basepath, "src", "monitor.py"),
        ]
        for f in files:
            sftp.put(f, os.path.join(self.REMOTE_PATH, os.path.basename(f)))

    def install_requirements(self):
        """TODO: Write unit tests"""
        requirements_file = os.path.join(self.REMOTE_PATH, "requirements.txt")
        cmds = ["sudo apt-get update",
                "apt-get install -y %s" % self.OS_PACKAGES,
                "virtualenv %s" % self.VENV_PATH,
                "source %s && pip install -r %s" % (
                    os.path.join(self.VENV_PATH, "bin", "activate"),
                    requirements_file
                )]
        for cmd in cmds:
            self.run_cmd(cmd)

    def run_collector_command(self):
        """TODO: Write unit tests"""
        site_url = settings.DATAFETCHER_SITE_URL
        key = settings.DATAFETCHER_CRYPT_KEY
        cmd_template = (
            'source %s && '
            'python %s'
            ' --source-ip="%s"'
            ' --server-url="%s"'
            ' --crypt-key="%s"'
        )
        cmd = cmd_template % (
            os.path.join(self.VENV_PATH, "bin", "activate"),
            os.path.join(self.REMOTE_PATH, "main.py"),
            self.host,
            site_url,
            key
        )
        self.run_cmd(cmd, get_pty=True)

    def execute_steps(self):
        """TODO: Write unit tests"""
        try:
            # Tries to run the command.
            # If there's an error, prepare the environment
            self.run_collector_command()
            return
        except:
            pass
        self.prepare_directory()
        self.upload_client()
        self.install_requirements()
        self.run_collector_command()
