# Client module

In this folder you can find the module sent to client machines to run and
send back to the server the monitored information.

## Run Tests
To run the tests, you should have `py.test` and `freezegun` modules installed.
Issue a `pip install pytest freezegun` and, after that, you can run the test suit doing:
`$ py.test src/tests`

## Module explanation

There's a `requirements.txt` file with the list of required Python dependencies
that should be installed before running the client code.

On `src/` folder, there are only two important files:

- main.py, which is the main script that should be run to collect data (run with `--help` for details)
- monitor.py, which is the file with the code needed to fetch system info and send to the server
