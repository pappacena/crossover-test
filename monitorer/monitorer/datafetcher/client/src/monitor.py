from __future__ import division
from Crypto.Cipher import AES
import platform
import requests
import json
import math
import psutil
import datetime


def encrypt(data, key):
    data = json.dumps(data)
    data = data.ljust(int(16 * math.ceil(len(data) / 16)))
    c = AES.new(key)
    return c.encrypt(data)


class Monitor:
    def __init__(self, source_ip):
        self.source_ip = source_ip

    def get_machine_name(self):
        return platform.node()

    def get_used_memory(self):
        """Percentage of used (non-freeable) memory"""
        mem = psutil.virtual_memory()
        return float(mem.total - mem.available) / mem.total * 100

    def get_cpu(self):
        """CPU percentage"""
        return psutil.cpu_percent(interval=1)

    def get_uptime(self):
        """Returns the total seconds since last boot"""
        boot = datetime.datetime.utcfromtimestamp(psutil.boot_time())
        return (datetime.datetime.now() - boot).total_seconds()

    def get_events(self):
        """
        TODO: This method needs testing on Windows machine.
        Reference: http://stackoverflow.com
                    /questions/11219213/read-specific-windows-event-log-event
        Returns a list of events using the following format:
        {
            "date": "2017-01-15 16:12",
            "category": "sys",
            "identifier": "123",
            "message": "System error warning"
        }"""
        try:
            import win32evtlog
        except ImportError:
            return []

        hand = win32evtlog.OpenEventLog("localhost", "Security")
        flags = (win32evtlog.EVENTLOG_BACKWARDS_READ |
                 win32evtlog.EVENTLOG_SEQUENTIAL_READ)

        ret = []
        for event in win32evtlog.ReadEventLog(hand, flags, 0):
            ret.append({
                "date": event.TimeGenerated,
                "category": event.EventCategory,
                "identifier": event.EventID,
                "message": "\n".join(event.StringInserts)
            })
        win32evtlog.CloseEventLog(hand)
        return []

    def get_content(self):
        return {
            "machine": {"ip_addr": self.source_ip,
                        "name": self.get_machine_name()},
            "metrics": {
                "cpu": self.get_cpu(),
                "memory": self.get_used_memory(),
                "uptime": self.get_uptime()
            },
            "winevents": self.get_events()
        }

    def send_data(self, server_url, crypt_key):
        url = "%s/api/v1/measure/" % server_url
        content = encrypt(self.get_content(), crypt_key)
        requests.post(url, data=content)
