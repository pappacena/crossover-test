import argparse
from monitor import Monitor

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Read monitoring indicators from this machine and send '
                    'the info to the central server'
    )

    parser.add_argument(
        '--crypt-key', nargs='?', required=True,
        help='Crypt key to use when communicating with the server',
        dest="crypt_key"
    )

    parser.add_argument(
        '--server-url', nargs='?', required=True,
        help='Server address where to send the info '
             '(ex: http://192.168.8.1:8888)',
        dest="server_url"
    )

    parser.add_argument(
        '--source-ip', nargs='?', required=True,
        help='IP through which the server connected to this machine',
        dest="source_ip"
    )

    args = parser.parse_args()
    m = Monitor(args.source_ip)
    m.send_data(args.server_url, args.crypt_key)
