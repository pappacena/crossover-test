from unittest import mock, TestCase
from freezegun import freeze_time
from .. import monitor


class MonitorTest(TestCase):
    @mock.patch.object(monitor.Monitor, "get_events")
    @mock.patch.object(monitor.Monitor, "get_uptime")
    @mock.patch.object(monitor.Monitor, "get_used_memory")
    @mock.patch.object(monitor.Monitor, "get_cpu")
    @mock.patch.object(monitor.Monitor, "get_machine_name")
    def test_get_content(self, machine_name, get_cpu, get_memory, get_uptime,
                         get_events):
        m = monitor.Monitor("192.168.8.1")
        self.assertEqual(m.get_content(), {
            "machine": {"ip_addr": "192.168.8.1",
                        "name": machine_name()},
            "metrics": {
                "cpu": get_cpu(),
                "memory": get_memory(),
                "uptime": get_uptime()
            },
            "winevents": get_events()
        })

    @mock.patch.object(monitor, "platform")
    def test_get_machine_name(self, platform):
        platform.node.return_value = "123"
        m = monitor.Monitor("192.168.8.1")
        self.assertEqual(m.get_machine_name(), "123")

    @mock.patch.object(monitor.psutil, "virtual_memory")
    def test_get_used_memory(self, virtual_memory):
        vm = mock.Mock()
        vm.available = 40
        vm.total = 100
        virtual_memory.return_value = vm
        m = monitor.Monitor("192.168.8.1")
        self.assertEqual(m.get_used_memory(), .6)

    @mock.patch.object(monitor.psutil, "cpu_percent")
    def test_get_cpu(self, cpu_percent):
        m = monitor.Monitor("192.168.8.1")
        self.assertEqual(m.get_cpu(), cpu_percent.return_value)

    @freeze_time("1970-01-01 00:00:45+0000")
    @mock.patch.object(monitor.psutil, "boot_time")
    def test_get_boot_time(self, boot_time):
        boot_time.return_value = 10
        m = monitor.Monitor("192.168.8.1")
        self.assertEqual(m.get_uptime(), 35)

    @mock.patch.object(monitor, "encrypt")
    @mock.patch.object(monitor, "requests")
    @mock.patch.object(monitor.Monitor, "get_content")
    def test_send_data(self, get_content, requests, encrypt):
        get_content.return_value = {
            "machine": {"ip_addr": "123"},
            "metrics": {
                "cpu": 1
            },
            "winevents": []
        }

        key = "1234567890123456"
        m = monitor.Monitor("192.168.8.1")
        m.send_data("http://foo.com", key)
        requests.post.assert_called_with(
            "http://foo.com/api/v1/measure/",
            data=encrypt.return_value
        )
        encrypt.assert_called_with(get_content.return_value, key)
