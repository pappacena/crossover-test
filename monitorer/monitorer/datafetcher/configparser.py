from xml.etree import ElementTree


def percent_str_to_float(string):
    return float(string.replace("%", ""))


class ClientConfig:
    def __init__(self):
        self.node = None
        self.ip = None
        self.user = None
        self.password = None
        self.email = None

    @classmethod
    def from_xml(cls, node):
        attrs = node.attrib
        c = cls()
        c.node = node
        c.ip = attrs["ip"]
        c.port = attrs["port"]
        c.username = attrs["username"]
        c.password = attrs["password"]
        c.email = attrs["mail"]
        return c

    def get_alarms(self):
        """Returns a dictionary with the alarms configured:
        {"cpu": 1, "memory": 2, ...}"""
        alarms = {}
        for node in self.node.iter("alert"):
            attrs = node.attrib
            alarms[attrs["type"]] = percent_str_to_float(attrs["limit"])
        return alarms


class ConfigParser:
    def __init__(self, file):
        self.file = file

    def __iter__(self):
        for e in ElementTree.fromstring(self.file.read()):
            yield ClientConfig.from_xml(e)
