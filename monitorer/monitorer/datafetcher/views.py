from django.utils.dateparse import parse_datetime
from django.http import HttpResponse
from datafetcher.models import Machine, NumericMeasure, WinEvent, METRIC_CODES
from datafetcher import request_crypto
from django.views.decorators.csrf import csrf_exempt
import json


@csrf_exempt
def save_measure(request):
    # decrypt body using our shared key
    try:
        data = request_crypto.get_json(request)
    except:
        resp = {"success": False, "errors": ["Invalid payload"]}
        return HttpResponse(json.dumps(resp), status=400)

    # Identify the machine
    machine = Machine.objects.get_or_create(
        ip_addr=data["machine"]["ip_addr"],
        defaults={"name": data["machine"].get("name")}
    )[0]

    # Save the list of metrics
    for metric_code, value in data["metrics"].items():
        if metric_code not in METRIC_CODES:
            resp = {
                "success": False,
                "errors": ["Invalid metric: %s" % metric_code]
            }
            return HttpResponse(json.dumps(resp), status=400)
        measure = NumericMeasure.objects.create(
            machine=machine,
            metric=METRIC_CODES[metric_code],
            value=value
        )
        machine.check_alerts(measure)

    # Save the events (not mandatory)
    # TODO: Write some validations
    event_list = data.get("winevents", [])
    for event in event_list:
        WinEvent.objects.create(
            machine=machine,
            category=event["category"],
            identifier=event["identifier"],
            message=event["message"],
            date=parse_datetime(event["date"])
        )

    resp = {"success": True}
    return HttpResponse(json.dumps(resp), status=201)
