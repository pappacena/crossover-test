from django.test import SimpleTestCase
from unittest import mock
from datafetcher.request_crypto import encrypt, decrypt, get_json, _add_padding


class RequestCryptoTest(SimpleTestCase):
    def test_string_padding(self):
        self.assertEqual(_add_padding("123456789"), "123456789" + " " * 7)

    def test_string_padding_longer_than_16(self):
        string = "1234567890" * 3
        self.assertEqual(_add_padding(string), string + " " * 2)

    def test_encode_decode_dict(self):
        data = {"test": "foo"}

        request = mock.Mock()
        encoded = encrypt(data)
        request.body = encoded

        self.assertNotEqual(encoded, data)
        self.assertEqual(decrypt(encoded), data)
        self.assertEqual(get_json(request), data)
