from django.test import SimpleTestCase
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from datafetcher.configparser import ConfigParser, percent_str_to_float


class ConfigParserTest(SimpleTestCase):
    def test_percent_str_to_float(self):
        self.assertAlmostEqual(percent_str_to_float("85%"), 85.)
        self.assertAlmostEqual(percent_str_to_float("95.4%"), 95.4)
        self.assertAlmostEqual(percent_str_to_float("195.4%"), 195.4)

    def test_iterate_over_configs(self):
        content = """
        <clients>
          <client ip="127.0.0.1" port="22" username="user"
                  password="password" mail="asa@asda.com">
          </client>
          <client ip="192.168.8.1" port="23" username="user1"
                  password="password1" mail="asa1@asda.com">
              <alert type="memory" limit="50%" />
              <alert type="cpu" limit="20%" />
          </client>
        </clients>"""
        fd = StringIO(content)
        parsed = list(ConfigParser(fd))
        self.assertEqual(len(parsed), 2)

        a, b = parsed
        self.assertEqual(a.ip, "127.0.0.1")
        self.assertEqual(a.port, "22")
        self.assertEqual(a.username, "user")
        self.assertEqual(a.password, "password")
        self.assertEqual(a.email, "asa@asda.com")
        self.assertEqual(a.get_alarms(), {})

        self.assertEqual(b.ip, "192.168.8.1")
        self.assertEqual(b.port, "23")
        self.assertEqual(b.username, "user1")
        self.assertEqual(b.password, "password1")
        self.assertEqual(b.email, "asa1@asda.com")
        self.assertEqual(b.get_alarms(), {'memory': 50, 'cpu': 20})
