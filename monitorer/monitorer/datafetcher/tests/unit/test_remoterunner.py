from django.test import SimpleTestCase, override_settings
from unittest import mock
from datafetcher.remoterunner import RemoteRunner


class RemoteRunnerTest(SimpleTestCase):
    @mock.patch("datafetcher.remoterunner.paramiko")
    def test_connect(self, paramiko):
        r = RemoteRunner("localhost", 22, "ausername", "apassword")
        self.assertEqual(r.connection, paramiko.SSHClient.return_value)
        paramiko.SSHClient.assert_called_with()
        r._connection.set_missing_host_key_policy.assert_called_with(
            paramiko.WarningPolicy()
        )
        r._connection.connect.assert_called_with(
            "localhost", port=22, username="ausername", password="apassword"
        )

    @override_settings(DATAFETCHER_CLIENT_PATH="/loc/f")
    @mock.patch.object(RemoteRunner, "connection")
    def test_upload_client_success_case(self, conn):
        r = RemoteRunner("localhost", 22, "ausername", "apassword")
        r.REMOTE_PATH = "/remote/"
        r.upload_client()

        conn.open_sftp.assert_called_with()
        self.assertEqual(
            conn.open_sftp.return_value.put.call_args_list,
            [mock.call("/loc/f/requirements.txt", "/remote/requirements.txt"),
             mock.call("/loc/f/src/main.py", "/remote/main.py"),
             mock.call("/loc/f/src/monitor.py", "/remote/monitor.py")]
        )

    @mock.patch.object(RemoteRunner, "run_cmd")
    def test_prepare_directory_success_case(self, run_cmd):
        r = RemoteRunner("localhost", 22, "ausername", "apassword")
        r.prepare_directory()
        self.assertEqual(
            r.run_cmd.call_args_list,
            [mock.call("rm -rf %s %s" % (r.REMOTE_PATH, r.VENV_PATH)),
             mock.call("mkdir -p %s" % r.REMOTE_PATH)]
        )
