from django.test import TestCase
from datafetcher.models import Machine, AlarmThreshold
from datafetcher.configparser import ClientConfig
from unittest import mock


class MachineModelTest(TestCase):
    def test_machine_representation_without_name(self):
        m = Machine()
        m.ip_addr = "192.168.1.123"
        self.assertEqual(str(m), "192.168.1.123")

    def test_machine_representation_with_name(self):
        m = Machine()
        m.ip_addr = "192.168.6.66"
        m.name = "batman machine"
        self.assertEqual(str(m), "batman machine")

    @mock.patch.object(ClientConfig, "get_alarms")
    def test_update_from_config(self, get_alarms):
        get_alarms.return_value = {"cpu": 0.5, "memory": 0.3, "uptime": 555}
        cfg = ClientConfig()
        cfg.email = "foo@email.com"

        m = Machine.objects.create(ip_addr="127.0.0.1")
        AlarmThreshold.objects.create(machine=m, metric=1, value=.33)

        m.sync_config(cfg)

        m.refresh_from_db()
        self.assertEqual(m.contact_email, "foo@email.com")
        self.assertEqual(m.alarmthreshold_set.count(), 3)

        a, b, c = list(m.alarmthreshold_set.order_by("metric"))

        self.assertEqual(a.metric, 1)
        self.assertEqual(a.value, 0.5)

        self.assertEqual(b.metric, 2)
        self.assertEqual(b.value, 0.3)

        self.assertEqual(c.metric, 3)
        self.assertEqual(c.value, 555)
