from django.test import TestCase
from django.test import Client
from datafetcher.models import Machine, NumericMeasure, WinEvent
from datafetcher.request_crypto import encrypt
import datetime
import pytz
import json


class SaveMeasureTest(TestCase):
    def test_fail_unencrypted_payload(self):
        data = {
            "machine": {"ip_addr": "192.168.8.1", "name": "test-machine"},
            "metrics": {
                "cpu": 0.87
            }
        }
        c = Client()
        resp = c.post("/api/v1/measure/", data=json.dumps(data),
                      content_type="application/json")
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(
            json.loads(resp.content),
            {"success": False, "errors": ["Invalid payload"]}
        )

    def test_fail_invalid_metric(self):
        data = {
            "machine": {"ip_addr": "192.168.8.1", "name": "test-machine"},
            "metrics": {
                "cpu": 0.87,
                "crazy metric": 123
            }
        }
        c = Client()
        resp = c.post("/api/v1/measure/", data=encrypt(data),
                      content_type="application/json")
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(
            json.loads(resp.content),
            {"success": False, "errors": ["Invalid metric: crazy metric"]}
        )

    def test_save_measure_basic_case(self):
        """Can I save a simple json with measures?"""
        data = {
            "machine": {"ip_addr": "192.168.8.1", "name": "test-machine"},
            "metrics": {
                "cpu": 0.87,
                "memory": 0.15,
                "uptime": 9999
            },
            "winevents": [
                {
                    "date": "2017-01-15 16:12",
                    "category": "sys",
                    "identifier": "123",
                    "message": "System error warning"
                },
                {
                    "date": "2017-01-15 16:13",
                    "category": "security",
                    "identifier": "222",
                    "message": "Security warning"
                }
            ]
        }

        content = encrypt(data)
        c = Client()
        resp = c.post("/api/v1/measure/", data=content,
                      content_type="application/json")
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(Machine.objects.count(), 1)
        machine = Machine.objects.get()
        self.assertEqual(machine.ip_addr, "192.168.8.1")
        self.assertEqual(machine.name, "test-machine")

        self.assertEqual(NumericMeasure.objects.count(), 3)
        cpu, memory, uptime = NumericMeasure.objects.order_by("metric")
        self.assertEqual(cpu.metric, 1)
        self.assertEqual(cpu.value, 0.87)

        self.assertEqual(memory.metric, 2)
        self.assertEqual(memory.value, 0.15)

        self.assertEqual(uptime.metric, 3)
        self.assertEqual(uptime.value, 9999)

        self.assertEqual(WinEvent.objects.count(), 2)
        evt1, evt2 = WinEvent.objects.order_by("date")

        self.assertEqual(evt1.category, "sys")
        self.assertEqual(evt1.identifier, "123")
        self.assertEqual(evt1.message, "System error warning")
        self.assertEqual(
            evt1.date,
            datetime.datetime(2017, 1, 15, 16, 12, tzinfo=pytz.utc)
        )

        self.assertEqual(evt2.category, "security")
        self.assertEqual(evt2.identifier, "222")
        self.assertEqual(evt2.message, "Security warning")
        self.assertEqual(
            evt2.date,
            datetime.datetime(2017, 1, 15, 16, 13, tzinfo=pytz.utc)
        )
