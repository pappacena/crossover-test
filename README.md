# Crossover assignment

Welcome to Crossover assessment. To get help on how to run the assessment, jump to
_How to start the server?_ section.


## About this assignment

The test environment for this assignment is built on top of Docker and docker-compose programs.

So, to test it, the only needed dependency is Docker with docker-compose. Everything (including the
test machines that should be monitored) should run without any further dependency.

The general architecture designed is composed by:

- A central server, responsible for keeping the clients configuration and starting
the data gathering from all network machines.

- A data fetching scripts, that is uploaded from the server to each one of the client machines,
using SSH (paramiko library).

The server is responsible for starting the data gathering by connecting in parallel to each
client machine, installing the dependencies of the client script (if they are not
installed yet) and running the script.

The client script collects system data, and do the encrypted HTTP request
(using pycrypto's AES cypher) to the central server with the collected information.

Then, the server saves this information and triggers the configured alarms, if necessary.

## Technologies used

The project server is enclosed in Docker containers (https://www.docker.com/).

The server was built using Python/Django, and all requirements are automatically installed
inside the container during the image build phase (depencencies described in file
`monitorer/requirements.txt`, with `Dockerfile` in the same directory).

The main dependencies used in the server are:

- Django for the webserver and database ORM
- Paramiko, for SSH connection
- gevent, for parallel execution of SSH command for several machines
- pycrypto, for HTTP payload encryptation (client-server communication)

The client script is defined at `monitorer/monitorer/datafetcher/client/`. It
only depends on pycrypt and requests library. For the client machine, the
encryptation key is preshared through SSH, which is a secure connection.

To configure the crypt key for the client, change on server's `settings.py` file
the variable `DATAFETCHER_CRYPT_KEY`.

The script to ask client machines to send monitored information is defined at
`monitorer/monitorer/datafetcher/management/commands/collect_data.py`. It uses
gevent to connect to all machines at the same time, since connecting to
100s of machines sequentially would be too slow. gevent was chosen because:

1- From the server point of view, most of the time that the script is running is I/O waiting,
that is, the client machine is doing his job and the server is just waiting for response.
2- paramiko library is not compatible with new async / await event loop on Python 3

To send the alarm e-mails without blocking the HTTP request, we are using a RabbitMQ container
as a queue server and a Celery worker container as a consumer, being the last one the responsible
for actually sending the e-mail.

## How to start the server?

In the first run, PostgreSQL container prepares the database cluster files in the
first run, do a `docker-compose up db` and kill the process once you see the line
_LOG:  database system is ready to accept connections_. This way, the webserver
will be able to connect to the database in the first run.

Do a `docker-compose up` on the root directory. This should build and launch the
server container together with the postgres database container.

At first run, after all containers are up and running, you should also execute
the initial database migration, with:

`docker-compose run --rm monitorer python manage.py migrate`

## How to run the test suit?

`docker-compose run --rm monitorer python manage.py test`

## How do I configure the client machines?

In the root level of the project, you can find a `config.xml` file, where you can
configure the list of client machines, alarms and contact e-mail.

For the assessment purpose, the file is already configured with the list of container
hosts that are automatically started when you do a `docker-compose up`. The file
is already mapped inside the server container.

## How to ask machines to collect?

Run the following command:

`docker-compose run --rm monitorer python manage.py collect_data --config=/usr/src/app/config.xml`

This might take a while at first run, since the script will install all required dependencies
in each client machine before running the command.

This will fetch all servers, save the preferences in database and connect to
each machine, uploading the script and running it (in parallel, using gevent + paramiko).

The result of the collected data in each machine is then sent using encrypted HTTP to
the server.

(you can see more information about the client script on `monitorer/datafetcher/client/README.md`)

## How can I see the collected values?

There's a command to show quickly the latest value for all machines at once:

`docker-compose run --rm monitorer python manage.py quick_report`

Besides that, you can query the database eigher using django shell
(`docker-compose run --rm monitorer python manage.py shell`) or connecting
directly to the PostgreSQL database shell (`docker-compose exec db psql -U postgres`).

## How can I test the e-mail sending?

Since Django is used as a webserver, I'm using it's e-mail component also (which runs on top of smtplib).

Create a file called `monitorer/monitorer/local_settings.py`, and put the following email configurations:

```
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'YOURUSERNAME@gmail.com'
EMAIL_HOST_PASSWORD = 'YOUR-PASSWORD'
EMAIL_USE_TLS = True
```

## TODO and Unfinished tasks:

- Put Django webserver to be run on gunicorn (`runserver` is not production ready)

- It's necessary to make `remoterunner.RemoteRunner` class compatible with Windows.
This is not the case, since I could not find a suitable Windows machine in the 3 days of test.

- The event log fetching implemented is not tested at all. It's not possible to test it
without a Windows machine.

- We should use a better packing method to upload the client scripts. Sending the ".py" files
with the requirements.txt, creating virtualenv and executing `pip install -r requirements.txt`
is not the best way of deploying the client agent.

- The class that executes the commands through SSH is not fully covered with unit tests,
but it's possible to see how it should be tested on `datafetcher/tests/unit/test_remoterunner.py`

- Needs to properly handle exceptions on `collect_data` command.

- Not all code is covered by tests, but the main parts are. We can improve it
by putting a coverage report also.


## Feedback

The test is well explained and is a fun job, but is quite big. I'm working full time
in a startup (and I bet most of good candidates are in this situation), and it is quite difficult
to find 10 free hours inside 3 days without compromising sleeping and/or the current job productivity
to do the job, write good explanation and record a video (!) of the project running.
